import {
  Component,
  OnInit,
  ElementRef
} from '@angular/core';
import { ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppConfigService, AuthenticationService, NodesApiService } from '@alfresco/adf-core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RowFilter, ShareDataRow } from '@alfresco/adf-content-services';
import { Node } from '@alfresco/js-api';

@Component({
  selector: 'aca-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})

export class FormComponent implements OnInit {
  
  PARENT_NODE: string;
  LOCALHOST: string = this.appConfig.getLocationHostname();
  URL_API: string;
  newDataForm;
  httpOptions;

  isLinear = true;
  docFormGroup: FormGroup;
  docUploadGroup: FormGroup;
  fileToUpload: File = null;
  uploadNextDis = true;
  uploadContentExist = false;
  uploadCompleted = false;
  spinnerOn = false;
  folderExist = false;
  date = new Date();
  folderFilter: RowFilter;
  children = [];
 
  @ViewChild('fileDropRef')
  myInputVariable: ElementRef;

  constructor(
    private http: HttpClient,
    private auth: AuthenticationService,
    private appConfig: AppConfigService,
    private nodesApi: NodesApiService
  ) {
      this.folderFilter = (row: ShareDataRow) => {
        let node = row.node.entry;
        
        if (node && node.isFolder && node.name=="testCpl") { //Possibilité d'ajouter plus de critère : 
            return true;
        }
        return false;
    };
  }

  ngOnInit() {
    this.uploadNextDis = true;
    this.uploadContentExist = false;
    this.uploadCompleted = false;
    this.spinnerOn = false;
    this.fileToUpload = null;

    this.docFormGroup = new FormGroup({
      nameDoc: new FormControl('', Validators.required),
      yearDoc: new FormControl(this.date.getFullYear(), Validators.required),
      monthDoc: new FormControl(this.date.getMonth()+1, Validators.required)
    });

    this.docUploadGroup = new FormGroup({
      docUpload: new FormControl('')
    });
  }

  fileBrowseHandler(event: Event) {
    const element = event.target as HTMLInputElement;
    this.fileToUpload = element.files.item(0);
    this.uploadNextDis =  false;
    this.uploadContentExist = true;
    this.uploadCompleted = true;
  }

  resetDocumentUpload() {
    this.uploadNextDis = true;
    this.uploadContentExist = false;
    this.uploadCompleted = false;
    this.fileToUpload = null;
    this.myInputVariable.nativeElement.value = '';
  }

  async valider() {

    let formData: FormData = new FormData();
    formData.append('filedata', this.fileToUpload, this.fileToUpload.name);
    formData.append('nodeType', 'cm:content');
    formData.append('year', this.docFormGroup.get('yearDoc').value);
    formData.append('month', this.docFormGroup.get('monthDoc').value);
    this.docFormGroup.patchValue({nameDoc:this.fileToUpload.name});

    this.uploadNextDis = true;
    this.spinnerOn = true;
    await this.createPDC(formData);
  }

  async createPDC(formData: FormData){
    var nameFolderYear = formData.get('year');
    var nameFolderMonth = formData.get('month');
    //recherche si un dossier année du même nom existe déjà 
    this.searchFolder(nameFolderYear);
    const result = await this.resolveAfter2Seconds();
    console.log(result);

    if (this.folderExist == true){
      //recherche si un dossier mois du même nom existe déjà
      this.searchFolder(nameFolderMonth);
      const result = await this.resolveAfter2Seconds();
      console.log(result);
      if (this.folderExist == true){
        console.log('le dossier mois existe');
        this.fileUpload(formData);
      }
      else {
        console.log('le dossier mois n existe pas');
        await this.createFolder(nameFolderMonth);
        this.fileUpload(formData);
      }
    }
    else {
      console.log("le dossier année existe ? "+this.folderExist);
      //create folder year
      await this.createFolder(nameFolderYear);
      //create folder month 
      await this.createFolder(nameFolderMonth);
      //upload file
      this.fileUpload(formData);
     }
  }

  searchFolder(nameFolder){
    var folder;
    this.folderExist = false;
    //Appel à l'API pour recupérer les éléments d'un dossier
    this.nodesApi.getNodeChildren(this.PARENT_NODE).subscribe(
      data => {
        this.children = data.list.entries;
        //Parcourt la liste des éléments du dossier
        for(let id=0; id < this.children.length; id++)
        {
          folder = this.children[id].entry;
          if (folder.name == nameFolder){ //s'il existe un dossier du même nom
            this.PARENT_NODE = folder.id;//on change l'id du dossier dans laquelle le doc doit être enregistré
            this.folderExist = true;
            return folder;
        }
      }
      //Soit le dossier ne contient pas d'élément (dossier vide), soit il n'y a pas de dossier ayant ce nom
      if (this.folderExist == false){
        return false;
      }
    },
    error => {
      console.log("error node year "+error);
      this.resetAll();
      alert('something bad happened. an error occurred while searching for a folder');
  });
}

  
  

  async createFolder(name){
    this.constructDataForm(name, 'cm:folder');
    this.folderUpload();
    const result = await this.resolveAfter2Seconds();
    console.log(result);
  }

  /**
   * fileUpload permet de gérer l'appel API pour importer le fichier sélectionné dans le formulaire
   * @param formdata 
   */
  fileUpload(formdata : FormData) {
    var url = this.constructUrl();
    this.constructHttpOptions();
    this.http.post(url, formdata, this.httpOptions).subscribe(
      data => {
        console.log('success'+data);
        this.resetAll();
      },
      error => {
        console.log(error);
        this.resetAll();
        alert('something bad happened. This file maybe already existing');
      }
    );
  }

  /**
   * folderUpload permet de gérer l'appel API pour créer un dossier
   */
  folderUpload(){
    this.constructHttpOptions();
    var url = this.constructUrl();
    this.http.post(url, this.newDataForm, this.httpOptions).subscribe(
      node => {
        console.log("folder created "+node['entry'].id);
        this.PARENT_NODE = node['entry'].id;
      },
      error => {
        console.log("An error is occured while folder's creation" + error);
        alert('something bad happened. an error occurred while creating a folder');
      }
    );
  }

  /**
   * Permet de MAJ le formulaire de données
   * @param name 
   * @param nodeType 
   */
  constructDataForm(name, nodeType){
    this.newDataForm = [
      {
        'name': name,
        'nodeType':nodeType
      }
    ]
  }
  
  /**
   * Permet de MAJ l'autorisation des tickets
   */
  constructHttpOptions(){
    let myAuth = this.auth.getTicketEcmBase64();
    this.httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: myAuth
      })
    };
  }

  constructUrl(){
    var url ='http://'+this.LOCALHOST+':8080/alfresco/api/-default-/public/alfresco/versions/1/nodes/'+this.PARENT_NODE+'/children';
    return url;
  }

  resolveAfter2Seconds() {
    return new Promise(resolve => {
    
    setTimeout(() => {
        resolve('resolved');
      }, 1000);
    });
  }
  onSelect(event: Node[]){
    if (event[0]!=null){
      this.PARENT_NODE = event[0].id;
    }
    return this.PARENT_NODE;
  }

  resetAll() {
    this.ngOnInit();
  }
}