import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn
} from '@angular/forms';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { AppConfigService, AuthenticationService } from '@alfresco/adf-core';
import { Router } from '@angular/router';

@Component({
  selector: 'aca-create-folder',
  templateUrl: './create-folder.component.html',
  styleUrls: ['./create-folder.component.scss']
})
export class CreateFolderComponent implements OnInit {
 //sur AWS le noderef du dossier Clients = ??
  PARENT_NODE: string = 'fd0e9592-8221-434c-b151-d67a6becc00a';
  LOCALHOST: string = this.appConfig.getLocationHostname(); 
  URL_API: string ='http://localhost:8080/alfresco/api/-default-/public/alfresco/versions/1/nodes/'+this.PARENT_NODE+'/children';
  folderFormGroup: FormGroup;
  isRequired: Boolean = false;
  required: ValidatorFn;

  isLinear = true;
  currentType: string;
  spinnerOn = false;
  validationDis = false;

  constructor(
    private http: HttpClient,
    private appConfig: AppConfigService,
    private auth: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit() {
    // We create the dialog form with validators control (see data validator in file app.effects.ts)
    // The creation form is generic
    this.spinnerOn = false;

    this.createForm();
  }

  createForm() {
    //déclarer le formulaire du dossier générique avec les propriétés qui le constituent
    this.folderFormGroup = new FormGroup({
      name: new FormControl('', Validators.required),
      refClient: new FormControl('', Validators.required)
    });
  }
  //This function serves to the redirection towards the node passed in parameter
  navigate(nodeId: string = null) {
    const routeUrl = '/personal-files';
    this.router.navigate([routeUrl, nodeId]);
  }

  //Here we treat all the informations received in the form and create the
  //nodebody to pass for the folder's creation function in createFolderAfterValidationForm()
  createFolderTreatment() {
    let formData = [
      {
        'name': this.folderFormGroup.get('name').value,
        'nodeType':'cm:folder',
        'description' : this.folderFormGroup.get('refClient').value
      }
    ]
    return(formData);
  }

  onSubmit() {
    let formData = this.createFolderTreatment();
    this.createFolderAfterValidationForm(this.URL_API, formData, this.auth.getTicketEcmBase64());

  }

  createFolderAfterValidationForm(url, formdata, ticket) {
    let myAuth = ticket
    let httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: myAuth
      })
    };
    this.http.post(url, formdata, httpOptions).subscribe(
      node => {
        this.resetAll();
        console.log("node: "+node);
      },
      error => {
        console.log("An error is occured while folder's creation" + error);
      }
    );
  }

  resetAll() {
    this.folderFormGroup.reset();

    Object.keys(this.folderFormGroup.controls).forEach(key => {
      this.folderFormGroup.controls[key].setErrors(null);
    });
  }
}
